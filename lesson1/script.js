"use strict";

/* 
*  Задать температуру в градусах по Цельсию. Вывести в alert 
*  соответствующую температуру в градусах по Фаренгейту. 
*  Подсказка: расчёт идёт по формуле: Tf = (9 / 5) * Tc + 32, 
*  где Tf – температура по Фаренгейту, Tc – температура по Цельсию
*/

function convertCtoF(tempCel){
  return (1.8 * tempCel + 32).toFixed(2)
}

function processTfield() {
  let tempCel = document.getElementsByName("temp")[0].value;
  if (tempCel == "") {
    alert("Введите температуру");
    return;
  }
  let tempFar = convertCtoF(tempCel);
  alert(`${tempFar} °F`);
}

//Объявить две переменные: admin и name
//name is deprecated ts(6385)
let admin;
let userName;

//Записать в name строку "Василий";
userName = "Василий";
admin = userName;
console.log(admin);

console.log(`Чему будет равно JS-выражение 1000 + "108"/: ${1000 + "108"}`);
/* defer ждёт загрузки DOM, async начинает работать сразу после того как прогрузилась
 *  defer используем для скриптов, работающих непосредственно с нашей страничкой
 *  async используем для сторонних скриптов типа счётчиков
 */

let a = 3;
while( a > 2){
    a--;
}
console.log(a);